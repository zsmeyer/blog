# Blog

## Introduction

I'm using this repository to store files for my [personal blog](https://zsmeyer.gitlab.io/blog). The files for the site are all in the public folder, and the source code is in the posts folder (and the index.qmd and about.qmd files). 

I'll try to keep any additional versions of the blog pages (such as Word docs or PDFs) in the root folder.

## Quarto

The blog is generated with [Quarto](https://quarto.org/), "an open-source scientific and technical publishing system built on Pandoc". I've only been using it for a few weeks (as of September 2022), but it has been wonderful so far. I highly recommend checking it out, even if your writing isn't primarily scientific or technical.
